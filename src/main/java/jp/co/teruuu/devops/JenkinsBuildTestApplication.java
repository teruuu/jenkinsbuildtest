package jp.co.teruuu.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsBuildTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JenkinsBuildTestApplication.class, args);
	}
	
	public void run(String... args) throws Exception {
    	System.out.println("処理開始" + this.getClass().getName());
        //アプリの処理
		System.out.println("hello jenkins build");
        System.out.println("処理終了");
    }
}
